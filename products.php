<?php
//Include DB configuration file
$dbHost = 'localhost';
$dbUsername = 'root';
$dbPassword = 'surya';
$dbName = 'web2';
//Connect with the database
$db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);

//Display error if failed to connect
if ($db->connect_errno) {
    printf("Connect failed: %s\n", $db->connect_error);
    exit();
}

//Set useful variables for paypal form
$paypalURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
$paypalID ='surya.valluri@live.com'; //Business Email

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
<link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="card.css">
    <title>OnShop</title>
    <style type="text/css">
  #footer {
               position : relative;
               bottom : 0;
               width: 100%;
               margin-top : 40px;
               margin-bottom:0px;
               overflow:hidden;
          }
  #cont   {
               min-height: 600px;
          }
        
        #head{
            margin-top: 60px;
        }    
        
  .card  {
               padding-left: 0px;
         }
</style>     
</head>

<body>
    
    <?php include 'header.php';?>
    
    <div id="cont">
<div class="container" >
    <div class="row justify-content-center">
                <div class="col-auto">
    <?php 
    echo "<div id='head'>";
    echo "<p class='h1'>Amount to be Paid</p>";
    echo "</div>";
    ?>
    <?php
    
    echo "<div class='card' style='width: 20rem;'>";
  echo "<div class='card-block'>";
    ?>
    <p class='card-text'><?php 
        
        $query14 = "SELECT SUM(price) from checkout where 1";
        $res14 = $db->query($query14);
  $result14=mysqli_fetch_array($res14);
        
        $result12 = $db->query("SELECT * FROM checkout");
    while($row12 = $result12->fetch_assoc()){
        echo " ".$row12['name']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    $".$row12['price']."";
        echo "</br>";
    }  echo "<hr>"; echo "<b>Total</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  $".$result14[0].""; ?></p>
  
<?php    echo "</div>";
 echo "</div>";

  ?>
    <div id="buy" style="margin-top:50px;">
        <?php
    //Fetch products from the database
$query10 = "SELECT SUM(price) from checkout where 1";
        $res10 = $db->query($query10);
  $result10=mysqli_fetch_array($res10);

$query11 = "UPDATE finalpay SET price =$result10[0] WHERE id = 500";
        $res11 = $db->query($query11);

     $results = $db->query("SELECT * FROM finalpay");
    while($row = $results->fetch_assoc())
         { ?>
                        
            <form action="<?php echo $paypalURL; ?>" method="post">
        <!-- Identify your business so that you can collect the payments. -->
        <input type="hidden" name="business" value="<?php echo $paypalID; ?>">
        
        <!-- Specify a Buy Now button. -->
        <input type="hidden" name="cmd" value="_xclick">
        
        <!-- Specify details about the item that buyers will purchase. -->
        <input type="hidden" name="item_name" value="<?php echo $row['name']; ?>">
        <input type="hidden" name="item_number" value="<?php echo $row['id']; ?>">
        <input type="hidden" name="amount" value="<?php echo $row['price']; ?>">
        <input type="hidden" name="currency_code" value="USD">
        
        <!-- Specify URLs -->
        <input type='hidden' name='cancel_return' value='http://localhost/cancel.php'>
        <input type='hidden' name='return' value='http://localhost/success.php'>
        
        <!-- Display the payment button. -->
        <input type="image" name="submit" border="0"
        src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">
        <img alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >
    </form>
                                  <?php       } ?>
        
    </div>
    
        </div>
    </div>
        </div>
    </div>
     <div id="footer">
    
    <?php include 'footer.php';?>
    </div>
     <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/tether/dist/js/tether.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    
    </body>
</html>






