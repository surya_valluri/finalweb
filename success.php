<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
<link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="card.css">
    <title>OnShop</title>
    <style type="text/css">
  #footer {
               position : relative;
               bottom : 0;
               width: 100%;
               margin-top : 40px;
               margin-bottom:0px;
               overflow:hidden;
          }
  #cont   {
               min-height: 600px;
          }
        #block{
            margin-top: 150px;
        }
        
  .card  {
               padding-left: 0px;
         }
</style>     
</head>

<body>
    
    <?php include 'header.php';?>
    
    <div id="cont">
<div class="container" >
    
    
    <div class="row justify-content-center">
                <div class="col-auto">
    <div id = block>
    <blockquote class="blockquote">
  <p class="mb-0">Transaction Successful</p>
</blockquote>
    </div>
        </div>
    </div>
        </div>
        
         <div class="row justify-content-center">
                <div class="col-auto">
                <a class="btn btn-md btn-block btn-primary" href='onshop.php' role="button">Continue Shopping</a>
                </div>
            </div>
        
        
        
        
        
        
        
        
        
        
        
        
        
    </div>
 <div id="footer">
    
    <?php include 'footer.php';?>
    </div>
     <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/tether/dist/js/tether.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    
    </body>
</html>
