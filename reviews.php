<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
<link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
    <title>OnShop</title>
    
    <style type="text/css">
  #footer {
    position : relative;
    bottom : 0;
      width: 100%;
    margin-top : 40px;
       margin-bottom:0px;
    overflow:hidden;
  }
        #cont{
            min-height: 600px;
            padding-top: 100px;
            padding-bottom: 100px;
        }
        #tit{
            padding-top: 30px;
        }
       
</style>
     
</head>

<body>
      <?php include 'header.php';?>
    <div id="tit">
    <div class="container">
    <h3> Our Customers Reviews</h3>
    </div>
    </div>

<div id="cont">
<div class="container" >
    
    <?php
     $db = mysqli_connect("localhost","root","surya","web2"); 
     $rev_search=mysqli_query($db, "SELECT * FROM `reviews` WHERE 1");
     while ($row4 = mysqli_fetch_array($rev_search)) 
 {
         $t = $row4['review'];
         $r = $row4['username'];
          echo "<blockquote class='blockquote' style='background:#BDBDBD;'>";
          echo "<p class='mb-0'>".$t."</p>";
          echo "<footer class='blockquote-footer'>".$r."</footer>";
         
        echo "</blockquote>";

              
     }
    ?>

   
    
    
    <form class="form-group" name="form2" method="post" action="refresh_review.php" >
    <label for="review" class="sr-only">Submit Your review</label>
    <input type="text" style="height:100px; width:500px;" class="form-control" name="review" placeholder="Enter Text" required autofocus>
        <div style="padding-top:10px;">
        <button type="submit" class="btn btn-primary" name="submit" id="submit">Submit</button>
        </div>
    </form>
   


    </div>
    </div>
    <div id="footer">
     <?php include 'footer.php';?>
    </div>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/tether/dist/js/tether.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    
    
 
    
    </body>

    
    
</html>
