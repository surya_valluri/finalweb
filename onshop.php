<?php
session_start();


$db = mysqli_connect("localhost","root","surya","web2"); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <link rel="stylesheet" href="./css/styles.css">
<link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css">
    
    <!--endbuild-->
    <title>OnShop</title>
</head>

<body>
      <?php include 'header.php';?>
    <div class="container">
   <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <img class="d-block img-fluid" src="img/pro/car1.jpeg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid" src="img/pro/car2.jpeg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid" src="img/pro/car3.jpeg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    </div>
    
    <div class="container">
        <div class="row row-content">
    <div class="card-deck">
  <div class="card">
    <img class="card-img-top" src="img/groc/veg1.jpeg" alt="Card image cap">
    <div class="card-block">
      <h4 class="card-title">Potatoes</h4>
      <p class="card-text">Price : Rs.20 per Kg</p>
        <p class="card-text"><small class="text-muted">Grab till stocks last</small></p>
        <?php $x=451; ?>
        <a class="btn btn-md btn-block btn-primary" href='product_view.php?id=<?php echo $x; ?>'role="button">View</a>
    </div>
  </div>
        
        <div class="card">
    <img class="card-img-top" src="img/groc/veg2.jpeg" alt="Card image cap">
    <div class="card-block">
      <h4 class="card-title">Onions</h4>
      <p class="card-text">Price : Rs.15 per Kg</p>
        <p class="card-text"><small class="text-muted">Grab till stocks last</small></p>
        <a class="btn btn-md btn-block btn-primary" href='product_view.php?id=452' role="button">View</a>
    </div>
  </div>
        
          <div class="card">
    <img class="card-img-top" src="img/groc/veg3.jpeg" alt="Card image cap">
    <div class="card-block">
      <h4 class="card-title">Carrots</h4>
      <p class="card-text">Price : Rs.25 per Kg</p>
        <p class="card-text"><small class="text-muted">Grab till stocks last</small></p>
        <a class="btn btn-md btn-block btn-primary" href='product_view.php?id=453' role="button">View</a>
    </div>
  </div>
        
          <div class="card">
    <img class="card-img-top" src="img/groc/veg4.jpeg" alt="Card image cap">
    <div class="card-block">
      <h4 class="card-title">Apples</h4>
      <p class="card-text">Price : Rs.120 per Kg</p>
        <p class="card-text"><small class="text-muted">Grab till stocks last</small></p>
        <a class="btn btn-md btn-block btn-primary" href='product_view.php?id=454' role="button">View</a>
    </div>
  </div>
        
          
   
    
       
    </div>
    </div>
         <div class="row row-content">
    <div class="card-deck">
        
        <div class="card">
    <img class="card-img-top" src="img/groc/veg9.jpeg" alt="Card image cap">
    <div class="card-block">
      <h4 class="card-title">Capsicum</h4>
      <p class="card-text">Price : Rs.50 per Kg</p>
        <p class="card-text"><small class="text-muted">Grab till stocks last</small></p>
        <a class="btn btn-md btn-block btn-primary" href='product_view.php?id=455' role="button">View</a>
    </div>
  </div>
        
        <div class="card">
    <img class="card-img-top" src="img/groc/veg6.jpeg" alt="Card image cap">
    <div class="card-block">
      <h4 class="card-title">Ginger</h4>
      <p class="card-text">Price : Rs.200 per Kg</p>
        <p class="card-text"><small class="text-muted">Grab till stocks last</small></p>
        <a class="btn btn-md btn-block btn-primary" href='product_view.php?id=456' role="button">View</a>
    </div>
  </div>
        
        <div class="card">
    <img class="card-img-top" src="img/groc/veg7.jpeg" alt="Card image cap">
    <div class="card-block">
      <h4 class="card-title">Cauliflower</h4>
      <p class="card-text">Price : Rs.30 per Kg</p>
        <p class="card-text"><small class="text-muted">Grab till stocks last</small></p>
        <a class="btn btn-md btn-block btn-primary" href='product_view.php?id=457' role="button">View</a>
    </div>
  </div>
        
        <div class="card">
    <img class="card-img-top" src="img/groc/veg8.jpeg" alt="Card image cap">
    <div class="card-block">
      <h4 class="card-title">Coriander</h4>
      <p class="card-text">Price : Rs.10 per Kg</p>
        <p class="card-text"><small class="text-muted">Grab till stocks last</small></p>
        <a class="btn btn-md btn-block btn-primary" href='product_view.php?id=458' role="button">View</a>
    </div>
  </div>
            </div>
        </div>
         <div class="row row-content">
    <div class="card-deck">
          <div class="card">
    <img class="card-img-top" src="img/pro/pro1.jpeg" alt="Card image cap">
    <div class="card-block">
      <h4 class="card-title">Besan</h4>
      <p class="card-text">Price : Rs.40 per Kg</p>
        <p class="card-text"><small class="text-muted">Grab till stocks last</small></p>
        <a class="btn btn-md btn-block btn-primary" href='product_view.php?id=459' role="button">View</a>
    </div>
  </div>
        
         <div class="card">
    <img class="card-img-top" src="img/pro/pro2.jpeg" alt="Card image cap">
    <div class="card-block">
      <h4 class="card-title">Maida</h4>
      <p class="card-text">Price : Rs.35 per Kg</p>
        <p class="card-text"><small class="text-muted">Grab till stocks last</small></p>
        <a class="btn btn-md btn-block btn-primary" href='product_view.php?id=460' role="button">View</a>
    </div>
  </div>
        
         <div class="card">
    <img class="card-img-top" src="img/pro/pro3.jpeg" alt="Card image cap">
    <div class="card-block">
      <h4 class="card-title">Cornflour</h4>
      <p class="card-text">Price : Rs.60 per Kg</p>
        <p class="card-text"><small class="text-muted">Grab till stocks last</small></p>
        <a class="btn btn-md btn-block btn-primary" href='product_view.php?id=461' role="button">View</a>
    </div>
  </div>
        
         <div class="card">
    <img class="card-img-top" src="img/pro/pro4.jpeg" alt="Card image cap">
    <div class="card-block">
      <h4 class="card-title">RiceAtta</h4>
      <p class="card-text">Price : Rs.40 per Kg</p>
        <p class="card-text"><small class="text-muted">Grab till stocks last</small></p>
        <a class="btn btn-md btn-block btn-primary" href='product_view.php?id=462' role="button">View</a>
    </div>
  </div>
            </div>
        </div>
</div>
    <?php include 'footer.php';?>
    
    <?php
  function runMyFunction() {
      
    header("Location: http://localhost/product_view.php");
  }

  if (isset($_GET['hello'])) {
    runMyFunction();
  }
?>


    
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/tether/dist/js/tether.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    
    </body>

    
    
</html>
